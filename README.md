# Best practices writing tests with React Testing Library

The purpose of this lightning talk is to go over best practices and common mistakes when writing tests with React Testing Library. Just by analyzing these bad habits and going over few recommendations, we can drastically improve:
- a11y of our components
- maintainability of our tests
- confidence in our tests

# About me
- Joined Hotjar recently 👋
- Not an a11y expert
- Part of Testing Library organization
- Author of eslint-plugin-testing-library

