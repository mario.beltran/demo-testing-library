import { useState } from 'react';
import { saveForm } from '../utils';

const ExampleA = () => {
  const [firstName, setFirstName] = useState('Mario');
  const [lastName, setLastName] = useState('Beltrán');
  const [fullName, setFullName] = useState('Mario Beltrán');
  const [isEditing, setIsEditing] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleCancel = () => {
    setIsEditing(false);
    const [prevFirstName, prevLastName = ''] = fullName.split(' ');
    setFirstName(prevFirstName);
    setLastName(prevLastName);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    const newFullName = await saveForm({ firstName, lastName });

    setFullName(newFullName);
    setIsLoading(false);
    setIsEditing(false);
  };

  return (
    <div>
      <h1>{isEditing ? 'Edit' : 'Your'} Profile</h1>
      {isEditing ? (
        <form onSubmit={handleSubmit}>
          <div>
            <label>
              First name
              <input
                type="text"
                value={firstName}
                onChange={(event) => setFirstName(event.target.value)}
              />
            </label>
          </div>
          <div>
            <label htmlFor="last-name">Last name</label>
            <input
              type="text"
              value={lastName}
              onChange={(event) => setLastName(event.target.value)}
              id="last-name"
            />
          </div>
          <div>
            <button type="button" onClick={handleCancel} disabled={isLoading}>
              Cancel
            </button>
            <button
              type="submit"
              disabled={isLoading}
              data-testid="submit-button"
            >
              {isLoading ? 'Saving...' : 'Edit Profile'}
            </button>
          </div>
        </form>
      ) : (
        <div>
          <button
            type="button"
            onClick={() => setIsEditing(true)}
            data-testid="edit-profile"
          >
            Edit
          </button>
          <p>
            <strong>Full name: </strong>
            {fullName}
          </p>
        </div>
      )}
    </div>
  );
};

export default ExampleA;
