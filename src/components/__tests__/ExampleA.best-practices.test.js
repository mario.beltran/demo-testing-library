import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ExampleA from '../ExampleA';

it('should edit data correctly', async () => {
  render(<ExampleA />);

  // ✅ 3) make explicit asserts
  // ✅ 4) use exhaustive assertion
  expect(screen.getByText('Your Profile')).toBeInTheDocument();
  expect(screen.getByText('Mario Beltrán')).toBeInTheDocument();

  userEvent.click(screen.getByText('Edit'));

  // ✅ 6) use accessible query
  const firstNameInput = screen.getByLabelText('First name');
  const lastNameInput = screen.getByLabelText('Last name');

  // ✅ 7) simulate user events with userEvent package
  userEvent.clear(firstNameInput);
  userEvent.type(firstNameInput, 'John');
  userEvent.clear(lastNameInput);
  userEvent.type(lastNameInput, 'Doe');

  // ✅ 6) use accessible query
  const submitButton = screen.getByRole('button', { name: /edit profile/i });

  // ✅ 5) avoid wrapping this simulated action with act. Instead, we wait
  // for corresponding UI element to appear with the following findBy query
  userEvent.click(submitButton);

  // ✅ 2) handle promise returned by findBy query
  const newFullName = await screen.findByText('John Doe');
  expect(newFullName).toBeInTheDocument();
});
