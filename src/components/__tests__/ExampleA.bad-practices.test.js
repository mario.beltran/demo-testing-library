import { render, screen, fireEvent, act } from '@testing-library/react';
import ExampleA from '../ExampleA';
import * as utils from '../../utils';

const saveFormSpy = jest.spyOn(utils, 'saveForm');

it('should edit data correctly', async () => {
  render(<ExampleA />);

  // ❌ 3) using getBy as explicit asserts
  screen.getByText('Your Profile');
  screen.getByText('Mario Beltrán');

  // ❌ 6) using ByTestId rather than accessible query
  fireEvent.click(screen.getByTestId('edit-profile'));

  // ❌ 7) using fireEvent methods
  fireEvent.input(screen.getByTestId('first-name-input'), {
    target: { value: 'John' },
  });
  fireEvent.input(screen.getByTestId('last-name-input'), {
    target: { value: 'Doe' },
  });

  // ❌ 5) using act combined with Testing Library utils
  await act(async () => {
    fireEvent.click(screen.getByTestId('submit-button'));
  });

  // ❌ 4) using vague assertions
  expect(saveFormSpy).toHaveBeenCalled();

  // ❌ 4) alternative version of vague assertions
  // ❌ 2) not handling promise returned by findBy
  const editButton = screen.findByTestId('edit-profile');
  expect(editButton);
});
