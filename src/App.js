import './App.css';
import ExampleA from "./components/ExampleA";

function App() {
  return (
    <main className="App">
      <ExampleA />
    </main>
  );
}

export default App;
